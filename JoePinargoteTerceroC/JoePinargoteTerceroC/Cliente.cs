﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JoePinargoteTerceroC
{
    class Cliente
    {
        //Datos del cliente
        private String apellidos;
        private String nombres;
        private String numeroCedula;
        private String direccion;

        //constructor
        public Cliente()
        {
            apellidos = " ";
            nombres = " ";
            numeroCedula = " ";
            direccion = " ";
        }
        public String Apellidos
        {
            get
            {
                return apellidos;
            }
            set
            {
                apellidos = value;
            }
        }

        public String Nombres
        {
            get
            {
                return nombres;
            }
            set
            {
                nombres = value;
            }
        }

        public String NumeroCedula
        {
            get
            {
                return numeroCedula;
            }
            set
            {
                numeroCedula = value;
            }
        }

        public String Direccion
        {
            get
            {
                return direccion;
            }
            set
            {
                direccion = value;
            }
        }
    }
}
