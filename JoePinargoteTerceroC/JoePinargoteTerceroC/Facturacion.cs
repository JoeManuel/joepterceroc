﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JoePinargoteTerceroC
{
    class Facturacion
    {
        private const double iva = 0.12;

        Gasolina gasolina = new Gasolina();
        Cliente cliente = new Cliente();

        public void Datos()//se piden los datos del consumidor y del producto(gasolina)
        {
            Console.WriteLine("Ingrese sus nombres: ");
            cliente.Nombres = Console.ReadLine();
            Console.WriteLine("Ingrese sus apellidos:");
            cliente.Apellidos = Console.ReadLine();
            Console.WriteLine("Ingrese su número de cédula:");
            cliente.NumeroCedula = Console.ReadLine();
            Console.WriteLine("Ingrese su dirección:");
            cliente.Direccion = Console.ReadLine();

            Console.WriteLine("Que tipo de galonina desea? : extra o super");
            gasolina.TipoGasolina = Console.ReadLine();
            Console.WriteLine("Cuantos galones desea comprar? :");
            gasolina.CantidadGalones = int.Parse(Console.ReadLine());
        }

        public double Subtotal()//se calcula el precio a pagar sin iva
        {


            double precioGalon = 0.0;

            if (gasolina.TipoGasolina == "extra" || gasolina.TipoGasolina == "EXTRA" || gasolina.TipoGasolina == "Extra")
            {
                precioGalon = gasolina.PrecioExtra * gasolina.CantidadGalones;
            }
            else
            {
                if (gasolina.TipoGasolina == "super" || gasolina.TipoGasolina == "SUPER" || gasolina.TipoGasolina == "Super")
                {
                    precioGalon = gasolina.PrecioSuper * gasolina.CantidadGalones;
                }
            }
            return precioGalon;
        }
        public double CalculaIva()//se calcula el valor del iva
        {
            double totalIva = 0.0;
            totalIva = Subtotal() * iva;
            return totalIva;
        }
        public double Total()//se calcula el total a pgar con iva
        {
            double total = 0.0;
            total = CalculaIva() + Subtotal();
            return total;
        }

        public void Mostrar()// se muestran los resultados
        {
            Console.WriteLine("\nconsumidor: {0} {1}", cliente.Apellidos, cliente.Nombres);
            Console.Write("Número de cédula: {0}", cliente.NumeroCedula + "  ");
            Console.WriteLine("Direccion: {0}", cliente.Direccion);
            Console.WriteLine("--------------------------");
            Console.Write("Tipo de gasolina: {0}", gasolina.TipoGasolina + "  ");
            Console.WriteLine("Cantidad de galones: {0}", gasolina.CantidadGalones);
            Console.WriteLine("--------------------------");
            Console.WriteLine("Subtotal: {0:C}", Subtotal());
            Console.WriteLine("Iva     : {0:C}", CalculaIva());
            Console.WriteLine("Total   : {0:C}", Total());
            Console.WriteLine("==========================");
        }
    }

}
