﻿using System;

namespace JoePinargoteTerceroC
{
    class Program
    {
        static void Main(string[] args)
        {
            Facturacion factura = new Facturacion();//creamos el objeto de la clase facturacion
            
            //usamos los metodos a traves del objeto creado
            factura.Datos();
            factura.Subtotal();
            factura.CalculaIva();
            factura.Total();
            factura.Mostrar();
        }
    }
}
